import 'package:flutter/material.dart';
import 'package:flutter_news_app/EventsTabs.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_news_app/OnboardingScreens/OnBoarding.dart';
import 'NewsTabs.dart';
import 'PodcastTabs.dart';

void main() async {
  Widget _firstScreen;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isSeen = prefs.getBool('isSeen');
  if (isSeen == null || isSeen == false) {
    _firstScreen = OnBoarding();
  } else {
    _firstScreen = NewsTabs(country: 'za');
  }
  runApp(App(_firstScreen));
}

class App extends StatelessWidget {
  Widget _firstScreen;

  App(this._firstScreen);

  @override
  Widget build(BuildContext context) {
    return (MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'News',
      //theme: new ThemeData(primaryColor: Color.fromRGBO(58, 66, 86, 1.0)),
      theme: new ThemeData(primaryColor: Colors.black),
      home: _firstScreen,
      initialRoute: '/',
      routes: {
        //'/IntroPageView': (context) => HomePage(),
        '/NewsTab': (context) => new NewsTabs(country: 'za'),
        '/EventsTab': (context) => new EventsTabs(),
        '/PodcastTab': (context) => new NewsTabs(country: 'za')
        //'/EventsTab': (context) => new EventsTabs(),
        //'/PodcastTab': (context) => new PodcastTabs()
      },
    ));
  }
}