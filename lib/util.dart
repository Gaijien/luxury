class Util {
  String _apiKey = "4c861132e6ef43cc881ca462f8bff174";
  String _tokenKey = "";
  String _podCastApi = "";

  String get tokenKey => _tokenKey;

  String get apiKey => _apiKey;

  String get podCastApi => _podCastApi;
}